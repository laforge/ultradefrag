@echo off
echo Set common environment variables...
set VERSION=5,0,0,0
set VERSION2="5, 0, 0, 0\0"
set ULTRADFGVER=5.0.0
set RELEASE_STAGE=RC2
set UDVERSION_SUFFIX=5.0.0-RC2
set RELEASE_CANDIDATE=1
set WINDDKBASE=D:\WINDDK\3790~1.183
set WINSDKBASE=C:\Program Files\Microsoft SDKs\Windows\v6.1
set MINGWBASE=D:\Software\MinGWStudio\MinGW
set MINGWx64BASE=D:\Software\mingw64
set NSISDIR=D:\Software\Tools\NSIS
set SEVENZIP_PATH=C:\Program Files\7-Zip
set MSVSBIN=D:\Software\Microsoft Visual Studio\VC98\Bin
set ROSINCDIR=D:\MyDocs\ReactOS030\include
